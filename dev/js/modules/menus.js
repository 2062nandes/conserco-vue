export const mainmenu = [
  {
    href: 'mallas-olimpicas-y-enmallados.html',
    title: 'Mallas olímpicas y enmallados',
    submenu: [
      {
        title: 'Postes, mallas y alambre de púas',
        href: 'mallas-olimpicas-y-enmallados.html#'
      },
      {
        title: 'Postes, mallas, alambre de púas y cordón de Hº de base',
        href: 'mallas-olimpicas-y-enmallados.html#'
      },
      {
        title: 'Postes, mallas, alambre de púas, cordón de Hº de base y muro de ladrillo 50 cm de altura',
        href: 'mallas-olimpicas-y-enmallados.html#'
      }
    ],
  },
  {
    href: 'estructuras-metalicas.html',
    title: 'Estructuras metálicas',
    submenu: [
      {
        title: 'Tinglados tipo dos medias aguas',
        href : 'estructuras-metalicas.html#tinglados-tipo-dos-medias-aguas'
      },
      {
        title: 'Tinglados tipo parabólico',
        href : 'estructuras-metalicas.html#tinglados-tipo-parabólico'
      },
      {
        title: 'Tinglado con perfilería metálica',
        href : 'estructuras-metalicas.html#tinglado-con-perfilería-metálica'
      }
    ]
  },
  {
    href: 'concretos-de-hormigon.html',
    title: 'Concretos de hormigón',
    submenu: [
      {
        title: 'Postes de Hº',
        href: 'concretos-de-hormigon.html#postes'
      },
      {
        title: 'Viguetas',
        href: 'concretos-de-hormigon.html#viguetas',
      },
      {
        title: 'Pisos de Hº de alto tráfico',
        href: 'concretos-de-hormigon.html#pisos-de-hormigon-de-alto-trafico'
      }
    ]
  },
  {
    href: 'servicio-de-pintado-y-mantenimiento-de-pinturas.html',
    title: 'Servicios de pintado y mantenimiento de pinturas',
    submenu: [
      {
        title: 'Pintado de condominios y edificios',
        href: 'servicio-de-pintado-y-mantenimiento-de-pinturas.html#'
      },
      {
        title: 'Pintado de tanques e infraestructura industrial',
        href: 'servicio-de-pintado-y-mantenimiento-de-pinturas.html#'
      },
      {
        title: 'Remarcación vial',
        href: 'servicio-de-pintado-y-mantenimiento-de-pinturas.html#'
      }
    ]
  },
  {
    href: 'otros-productos-y-servicios.html',
    title: 'Otros productos y servicios',
    submenu: [
      {
        title: 'Soldadura industrial y domiciliaria',
        href: 'otros-productos-y-servicios.html#'
      },
      {
        title: 'Soldadura [Juegos infantiles, portones, barandas metálicas, verijas]',
        href: 'otros-productos-y-servicios.html#'
      },
      {
        title: 'Accesorios y enmallado [Seguros, tensores, torniquetes]',
        href: 'otros-productos-y-servicios.html#'
      },
      {
        title: 'Servicios eléctricos, industriales y domésticos',
        href: 'otros-productos-y-servicios.html#'
        //
      }
    ]
  }
];
export const menuinicio = [
  {
    title: 'Inicio',
    href: './',
  },
  {
    title: 'Nosotros',
    href: './nosotros.html',
  },
  {
    title: 'Contactos',
    href: './contactos.html',
  }
];