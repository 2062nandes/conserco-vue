export const slide = [
  {
    title : 'Mallas Olímpicas',
    href  : 'images/slide01.jpg',
  },
  {
    title: 'Estructuras Metálicas',
    href: 'images/slide02.jpg',
  },
  {
    title: 'Enmallados',
    href: 'images/slide03.jpg',
  },
  {
    title: 'Cerramiento de Garaje',
    href: 'images/slide04.jpg',
  },
  {
    title: 'Soldadura de juegos infantiles',
    href: 'images/slide05.jpg',
  },
  {
    title: 'Tinglados',
    href: 'images/slide06.jpg',
  },
  {
    title: 'Refacción y pintado',
    href: 'images/slide07.jpg',
  },
  {
    title: 'Piso de hormigón de alto tráfico',
    href: 'images/slide08.jpg',
  }
];