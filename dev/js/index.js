//Data vuejs
import { contactform } from './modules/contactform'
import { menuinicio, mainmenu } from './modules/menus'
import { slide } from './modules/slider'

import Vue from 'vue/dist/vue.min'
// import Vue from 'vue/dist/vue'
import VueResource from 'vue-resource/dist/vue-resource.min'

Vue.use(VueResource);

new Vue({
  el: '#app',
  data: {
    toggle: false,
    formSubmitted: false,
    vue: contactform,
    menuinicio,
    mainmenu,
    slide,
    slideshow: 'images/slide01.jpg'
  },
  created: () => {
    //FOOTER AÑO
    function getDate() {
      var today = new Date()
      var year = today.getFullYear()
      document.getElementById('currentDate').innerHTML = year
    }
    getDate()
  },
  methods: {
    isFormValid: function () {
      return this.nombre != ''
    },
    submitForm: function () {
      if (!this.isFormValid()) return
      this.formSubmitted = true
      this.$http.post('/mail.php', { vue: this.vue }).then(function (response) {
        // console.log(response)
        this.vue.envio = response.data
        this.clearForm()
      }, function () { })
    },
    clearForm: function () {
      this.vue.nombre = ''
      this.vue.email = ''
      this.vue.telefono = ''
      this.vue.movil = ''
      this.vue.direccion = ''
      this.vue.ciudad = ''
      this.vue.mensaje = ''
      this.vue.formSubmitted = false
    },
    toggleMenu: function () {
      if (this.toggle == true) {
        this.toggle = false
        console.log('DESACTIVADO')
      }
      else {
        this.toggle = true
        console.log('ACTIVO')
      }
    },
    showSlider: function (a) {
      this.slideshow = a;
    }
  }
})
